################################################################################################################################################
# FAIME Parallel Policy for NO PARALLELISM.  All code is run sequentially!
# This code defines functions conforming to the FAIME Parallel Policy interface
################################################################################################################################################

#################################
# Interface
#################################

#' Initializes sequential library
#'
#' @param handle A list containing the parallel policy interface created via \code{\link{faimeParallelCreateHandle.none}}
#' @param \dots Additonal arguments for initializing the parallelization library
#' @return Result of initializing parallelization library
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelInit.none <- function(handle, ...) {
  # no-op
}

#' Stops sequential library
#'
#' @param handle A list containing the parallel policy interface created via \code{\link{faimeParallelCreateHandle.none}}
#' @param \dots Additonal arguments for stopping the parallelization library
#' @return Result of stopping parallelization library
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelStop.none <- function(handle, ...) {
  # no-op
}

#' Sequential lapply
#'
#' @param handle A list containing the parallel policy interface created via \code{\link{faimeParallelCreateHandle.none}}
#' @param \dots Additonal arguments to \code{\link{lapply}}
#' @return see \code{\link{lapply}}
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelLapply.none <- function(handle, ...) {
  # forward to sequential R-standard
  lapply(...)
}

#' Sequential sapply
#'
#' @param handle A list containing the parallel policy interface created via \code{\link{faimeParallelCreateHandle.none}}
#' @param \dots Additonal arguments to \code{\link{sapply}}
#' @return see \code{\link{sapply}}
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelSapply.none <- function(handle, ...) {
  sapply(...)
}

#' Sequential apply
#'
#' @param handle A list containing the sequential policy interface created via \code{\link{faimeParallelCreateHandle.none}}
#' @param \dots Additonal arguments to \code{\link{apply}}
#' @return see \code{\link{apply}}
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelApply.none <- function(handle, ...) {
  # forward to sequential R-standard
  apply(...)
}

#' Create interface handle
#'
#' @param nCores The number of cores to use (not used)
#' @param \dots Any additional parameters to bind to the handle
#' @return A list containing the parallel policy interface
#' @references
#' PLoS Computational Biology: Single Sample Expression-Anchored Mechanisms Predict Survival in Head and Neck Cancer
#' @export
faimeParallelCreateHandle.none <- function(nCores, ...) {
  list(fInit=faimeParallelInit.none,
       fStop=faimeParallelStop.none,
       fLapply=faimeParallelLapply.none,
       fSapply=faimeParallelSapply.none,
       fApply=faimeParallelApply.none,
       nCores=1,
       ...)
}
