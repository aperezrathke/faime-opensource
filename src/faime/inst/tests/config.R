################################################################################################################################################
# Common configuration routines
################################################################################################################################################

#################################
# Libraries
#################################

# install.packages("parallel")
library("parallel")
# Cache number of cores
internalConfigParallelNCores <- detectCores()
# Detach as we may already be using Snow
detach("package:parallel")

# Load FAIME
library("faime")

#################################
# Parallelization Config
#################################

#' @return TRUE if Snowfall parallelization is enabled
configParallelIsSf <- function() {
  TRUE
}

#' @return TRUE if parallelization is disabled
configParallelIsNone <- function() {
  !configParallelIsSf()
}

#' @return Number of cores to use
configParallelNCores <- function() {
  internalConfigParallelNCores
}

# Determine parallelization policy
if ( configParallelIsSf() ) {
  # Load Snowfall parallelization policy
  configParallelCreateHandleFactory <- faimeParallelCreateHandle.sf
  configParallelCreateHandleParams <- list(nCores=configParallelNCores())
  configParallelInitParams <- list(parallel=TRUE)
  configParallelStopParams <- list()
} else {
  # Default: Load sequential policy
  configParallelCreateHandleFactory <- faimeParallelCreateHandle.none
  configParallelCreateHandleParams <- list(nCores=1)
  configParallelInitParams <- list()
  configParallelStopParams <- list()
}

#' Utility which creates parallel policy handle and initializes
#' @return hpc - a handle to a parallelization policy (post init)
configParallelSetUpUtil <- function() {
  # Create parallelization handle
  params <- configParallelCreateHandleParams
  hpc <- do.call(configParallelCreateHandleFactory, params)
  # Initialize parallelization
  params <- configParallelInitParams
  params$handle <- hpc
  do.call(hpc$fInit, params)
  return(hpc)
}

#' Utility to clean up parallelization policy
#' @param handle handle to parallelization policy
configParallelTearDownUtil <- function(handle) {
  params <- configParallelStopParams
  params$handle <- handle
  do.call(handle$fStop, params)
}

#' Intializes high performance computing (hpc) handle and makes globally accessible
configGlobalSetupParallel <- function() {
  # Insert any global initialization code here
  hpc <<- configParallelSetUpUtil()
}

#' Tears down global hpc handle
configGlobalTearDownParallel <- function() {
  # Insert any clean up code here
  configParallelTearDownUtil(hpc)
}

#################################
# Test Params Config
#################################

#' Utility appends Transform Negative Exponent specific test parameters:
#' - transform
#'  - transformName
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.TrNegExp <- function(testParams) {
  testParams$transform <- faimeTransform.RankedNegExp
  testParams$transformName <- "TrNegExp"
  return(testParams)
}

#' Utility appends Transform Positive Exponent specific test parameters:
#' - transform
#' - transformName
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.TrPosExp <- function(testParams) {
  testParams$transform <- faimeTransform.RankedPosExp
  testParams$transformName <- "TrPosExp"
  return(testParams)
}

#' Utility appends GO-MF specific test parameters:
#' - ontologyName
#' - ontologyPath
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.GOMF <- function(testParams) {
  testParams$ontologyName <- "GOMF"
  testParams$ontologyPath <- configGetOntologyPath.GOMF()
  return(testParams)
}

#' Utility appends GO-BP specific test parameters:
#' - ontologyName
#' - ontologyPath
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.GOBP <- function(testParams) {
  testParams$ontologyName <- "GOBP"
  testParams$ontologyPath <- configGetOntologyPath.GOBP()
  return(testParams)
}

#' Utility appends GO-BP-500-5 specific test parameters:
#' - ontologyName
#' - ontologyPath
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.GOBP.500.5 <- function(testParams) {
  testParams$ontologyName <- "GOBP-500-5"
  testParams$ontologyPath <- configGetOntologyPath.GOBP.500.5()
  return(testParams)
}

#' Utility appends KEGG specific test parameters:
#' - ontologyName
#' - ontologyPath
#' @param testParams Parameters that will be modified
#' @return \code{testParams} with added attributes
appendTestParams.KEGG <- function(testParams) {
  testParams$ontologyName <- "KEGG"
  testParams$ontologyPath <- configGetOntologyPath.KEGG()
  return(testParams)
}
