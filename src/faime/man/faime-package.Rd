\docType{package}
\name{faime-package}
\alias{faime}
\alias{faime-package}
\title{FAIME: Functional Analysis of Individual Microarray Expression}
\description{
  FAIME: Functional Analysis of Individual Microarray
  Expression
}
\details{
  \tabular{ll}{ Package: \tab faime\cr Type: \tab
  Package\cr Version: \tab 1.0\cr Date: \tab 2012-06-12\cr
  License: \tab GPL (>= 2)\cr LazyLoad: \tab yes\cr }

  Transform gene expression data (microarray or RNA-seq)
  into pathway scores which can then be analyzed using
  differential analysis tools such as SAM or GSEA.

  \code{\link{faimeRun}} is the main application entry
  point.
}
\author{
  Lussier Lab \email{perezrat@uic.edu}
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}
\seealso{
  \code{\link{faimeRun}}
}
\keyword{package}

