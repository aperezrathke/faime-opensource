\name{faimeParallelCreateHandle.sf}
\alias{faimeParallelCreateHandle.sf}
\title{Create interface handle}
\usage{
  faimeParallelCreateHandle.sf(nCores, ...)
}
\arguments{
  \item{nCores}{The number of cores to use}

  \item{\dots}{Any additional parameters to bind to the
  handle}
}
\value{
  A list containing the parallel policy interface
}
\description{
  Create interface handle
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

