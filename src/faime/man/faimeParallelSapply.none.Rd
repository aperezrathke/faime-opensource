\name{faimeParallelSapply.none}
\alias{faimeParallelSapply.none}
\title{Sequential sapply}
\usage{
  faimeParallelSapply.none(handle, ...)
}
\arguments{
  \item{handle}{A list containing the parallel policy
  interface created via
  \code{\link{faimeParallelCreateHandle.none}}}

  \item{\dots}{Additonal arguments to \code{\link{sapply}}}
}
\value{
  see \code{\link{sapply}}
}
\description{
  Sequential sapply
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

