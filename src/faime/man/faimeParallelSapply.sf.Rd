\name{faimeParallelSapply.sf}
\alias{faimeParallelSapply.sf}
\title{Parallel sapply}
\usage{
  faimeParallelSapply.sf(handle, ...)
}
\arguments{
  \item{handle}{A list containing the parallel policy
  interface created via
  \code{\link{faimeParallelCreateHandle.sf}}}

  \item{\dots}{Additonal arguments to \code{\link{sapply}}}
}
\value{
  see \code{\link{apply}}
}
\description{
  Parallel sapply
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

