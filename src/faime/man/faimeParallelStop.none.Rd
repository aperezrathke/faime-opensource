\name{faimeParallelStop.none}
\alias{faimeParallelStop.none}
\title{Stops sequential library}
\usage{
  faimeParallelStop.none(handle, ...)
}
\arguments{
  \item{handle}{A list containing the parallel policy
  interface created via
  \code{\link{faimeParallelCreateHandle.none}}}

  \item{\dots}{Additonal arguments for stopping the
  parallelization library}
}
\value{
  Result of stopping parallelization library
}
\description{
  Stops sequential library
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

