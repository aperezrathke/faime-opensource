\name{faimeReduce.CentroidDistance}
\alias{faimeReduce.CentroidDistance}
\title{Computes distance between centroid of target and non-target scores.}
\usage{
  faimeReduce.CentroidDistance(transformedScores,
    targetIds, nonTargetIds)
}
\arguments{
  \item{transformedScores}{A vector of transformed
  expression values with names}

  \item{targetIds}{A vector of names associated with the
  gene set of interest}

  \item{nonTargetIds}{A vector of names not-associated with
  the gene set of interest}
}
\value{
  A FAIME score assigned to the ontology mapping for this
  sample: the centroid distance
}
\description{
  Computes distance between centroid of target and
  non-target scores.
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

