\name{faimeTransform.RankedNegExp}
\alias{faimeTransform.RankedNegExp}
\title{Transforms expression data to be ranked and modulated by a negative exponent}
\usage{
  faimeTransform.RankedNegExp(expData, hpc, na.last = TRUE)
}
\arguments{
  \item{expData}{An expression matrix (or data.frame), row
  for a gene and column for a sample}

  \item{hpc}{A handle to a parallel policy interface}

  \item{na.last}{See \code{\link{rank}} for controlling the
  treatment of NAs.}
}
\value{
  Ranked gene expressions modulated by e^-[0,1]
}
\description{
  Transforms expression data to be ranked and modulated by
  a negative exponent
}
\references{
  PLoS Computational Biology: Single Sample
  Expression-Anchored Mechanisms Predict Survival in Head
  and Neck Cancer
}

