################################################################################################################################################
# Utility to run Twilight on FAIME data
################################################################################################################################################

# Load Twilight
library("twilight")

# --------------

#' Utility function based on path convention for running a SAM analysis
#' @param lArgs A list of arguments containing dataSet.name, faimeData.mods, faimeData.class, faimeData.rdata.path, ontology.name, transform.funcName, output.dir, twilight.paired
#' @return Twilight output for arguments
runTwilight <- function( lArgs ) {
  # Determine output directory
  output.dir <- getParamsOutputDir(lArgs)
  condCreateDir(output.dir)
  
  # Determine full name of FAIME data
  faimeData.fullName <- getDataSetFaimeDataFullName(lArgs)
  
  # Load in FAIME scores
  faimeData.scores <- getDataSetFaimeDataScores(faimeData.fullName, lArgs)
  
  # Export FAIME Scores as plain text for readability and use in outside packages
  exportFaimeDataScores(faimeData.fullName, output.dir)
  
  # Read in phenotype information
  pheno.values <- readAndVerifyDataSetPhenoValues(lArgs$dataSet.name, faimeData.scores)
  
  # Run Twilight
  # @TODO - verify whether or not FAIME data should be log transformed
  print("Running Twilight:")
  yin <- as.numeric(as.matrix(pheno.values["TwilightClass",]))
  twilight.output <- twilight.pval(xin=faimeData.scores,
                                   yin=yin,
                                   method="z",
                                   paired=lArgs$twilight.paired,
                                   B=1000)
  print(twilight.output)
  
  # Write Twilight result
  twilight.basePathName <- sprintf("%s/%s-Twilight", output.dir, faimeData.fullName)
  save(twilight.output, file=sprintf("%s.rdata", twilight.basePathName))
  dput(twilight.output, file=sprintf("%s.txt", twilight.basePathName))
  write.csv(twilight.output$result, file=sprintf("%s-results.csv", twilight.basePathName))
  
  # Return Twilight output
  return(twilight.output)
}

#' Utility function which separates up and down regulated terms and filters by parameter FDR
#' @param twilight.output output from calling twilight.pval
#' @param max.fdr the maximum allowable false discovery rate; all adjusted p-values >= max.fdr are removed
#' @return a list with names:
#'       $all for all adjusted but unfiltered p-values,
#'       $lo for all filtered down-regulated p-values,
#'       and $up for all filtered up-regulated p-values
adjustAndFilterTwilightPValues <- function(twilight.output, max.fdr=0.05) {
  pvalues <- twilight.output$result$pvalue
  names(pvalues) <- rownames(twilight.output$result)
  pvalues <- p.adjust(pvalues, method="BY")
  out <- list(all=pvalues,
              lo=pvalues[(twilight.output$result$observed < twilight.output$result$expected)],
              up=pvalues[(twilight.output$result$observed >= twilight.output$result$expected)])
  out$lo <- out$lo[out$lo < max.fdr]
  out$up <- out$up[out$up < max.fdr]
  return(out)
}
