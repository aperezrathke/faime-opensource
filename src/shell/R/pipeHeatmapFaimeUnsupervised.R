################################################################################################################################################
# Research Pipeline: Generate FAIME Heatmaps
################################################################################################################################################

source("./src/shell/R/configPaths.R")
source("./src/shell/R/runFaime.R")
source("./src/shell/R/runHeatmapFaime.R")
source("./src/shell/R/runSam.R")
source("./src/shell/R/utilsGo.R")
source("./src/shell/R/utilsKegg.R")

# Mask used to filter which data sets to run FAIME
DATA.SET.MASK <- c("GSE2379"=FALSE,
                   "GSE6631"=FALSE,
                   "E-MEXP-44"=FALSE,
                   "GSE36968"=TRUE,
                   "GSE13861"=TRUE)

# The names of the datasets to run FAIME on
DATA.SET.NAMES <- names(DATA.SET.MASK[DATA.SET.MASK == TRUE])

# Mapping from data set to its modifiers
DATA.SET.MODS <- c("GSE2379"="-filtered",
                   "GSE6631"="-filtered",
                   "E-MEXP-44"="-filtered",
                   "GSE36968"="-rpkm",
                   "GSE13861"="-norm")

# Mask used to filter out which ontologies to use
ONTOLOGY.MASK <- c("KEGG"=TRUE,
                   "GO_molecular_function_6_14_12"=TRUE,
                   "GO_biological_process_6_14_12"=TRUE,
                   "GOMF-gene"=TRUE,
                   "GOBP-gene"=TRUE,
                   "GOBP-gene-500-5"=TRUE)

# The names of the ontologies to use
ONTOLOGY.NAMES <- names(ONTOLOGY.MASK[ONTOLOGY.MASK == TRUE])

# The transformations used to generate FAIME scores
TRANSFORM.MASK = c("faimeTransform.RankedNegExp"=TRUE,
                   "faimeTransform.RankedPosExp"=FALSE,
                   "faimeTransform.Ranked"=FALSE,
                   "faimeTransform.None"=FALSE)

# The names of the transformation functions
TRANSFORM.FUNC.NAMES <- names(TRANSFORM.MASK[TRANSFORM.MASK == TRUE])

# The reductions used to generate FAIME scores
REDUCE.MASK <- c("faimeReduce.CentroidDistance"=TRUE,
                 "faimeReduce.Centroid"=FALSE,
                 "faimeReduce.MedianDistance"=FALSE,
                 "faimeReduce.Median"=FALSE)

# The names of the reduction functions
REDUCE.FUNC.NAMES <- names(REDUCE.MASK[REDUCE.MASK == TRUE])

# Keys specifying which ontology path id to path name mapping to use
ONTOLOGY.ATTRIB.KEYS <- c("KEGG"="kegg",
                          "GO_molecular_function_6_14_12"="go",
                          "GO_biological_process_6_14_12"="go",
                          "GOMF-gene"="go",
                          "GOBP-gene"="go",
                          "GOBP-gene-500-5"="go")

ONTOLOGY.LABELS <- c("KEGG"="KEGG Pathways",
                     "GO_molecular_function_6_14_12"="GO Molecular Function",
                     "GO_biological_process_6_14_12"="GO Biological Process",
                     "GOMF-gene"="GO Molecular Function",
                     "GOBP-gene"="GO Biological Process",
                     "GOBP-gene-500-5"="GO Biological Process")

# The name of the column containing the ontology identifiers
PATHID.COL.NAMES <- c(go=goIdColName(),
                      kegg=keggIdColName())

# The name of the column containing the ontology identifiers
PATHNAME.COL.NAMES <- c(go=goNameColName(),
                        kegg=keggNameColName())

# Mask of clustering methods
HEAT.HCLUST.METHODS.MASK <- c("ward"=TRUE,
                              "single"=TRUE,
                              "complete"=TRUE,
                              "average"=TRUE,
                              "mcquitty"=TRUE,
                              "median"=TRUE,
                              "centroid"=TRUE,
                              "none"=TRUE)

# The final set of clustering methods to use
HEAT.HCLUST.METHODS <- names(HEAT.HCLUST.METHODS.MASK[HEAT.HCLUST.METHODS.MASK == TRUE])

#' @return 
transformDataSetLabels.GSE36968 <- function(dataSet.scores, pheno.values) {
  stage.map <- c("I"="1", "II"="2", "III"="3", "IV"="4", "Normal"="N")
  colnames(dataSet.scores) <- paste(colnames(dataSet.scores),
                                    " (",
                                    stage.map[as.matrix(pheno.values["Stage", ])],
                                    ")",
                                    sep="")
  list(dataSet.scores=dataSet.scores)
}

DATA.SET.TRANSFORM.LABELS.FUNCS <- list("GSE36968"=transformDataSetLabels.GSE36968)

# --------------
# Run analysis

# Variable for storing all overlap results
ov.results <- data.frame()

for (dataSet.name in DATA.SET.NAMES) {
  for (ontology.name in ONTOLOGY.NAMES) {
    for (transform.funcName in TRANSFORM.FUNC.NAMES) {
      for (reduce.funcName in REDUCE.FUNC.NAMES) {
        for (heat.hclust.method in HEAT.HCLUST.METHODS) {
          # Arguments for FAIME scores
          lArgs.scores <- list(dataSet.name=dataSet.name,
                               dataSet.inClass=getFaimeClass(),
                               dataSet.mods=DATA.SET.MODS[dataSet.name],
                               dataSet.outSubClass="unsupervised",
                               ontology.name=ontology.name,
                               transform.funcName=transform.funcName,
                               reduce.funcName=reduce.funcName,
                               transformDataSetLabels=DATA.SET.TRANSFORM.LABELS.FUNCS[[dataSet.name]])
          
          # Create output arguments
          lArgs.sig <- lArgs.scores
          lArgs.sig$heat.unsupervised <- TRUE
          lArgs.sig$heat.hclust.method <- heat.hclust.method
          lArgs.sig$heat.main <- paste(dataSet.name, ONTOLOGY.LABELS[ontology.name], sep=": ")
          
          # Determine clustering method
          lArgs.sig$heat.Rowv <- NA
          clust.key <- "heat.Colv"
          clust.val <- NA
          if (heat.hclust.method != "none") {
            clust.key <- "heat.hclustfun"
            clust.val <- function(d, members=NULL) { hclust(d, method=heat.hclust.method, members) }
          }
          lArgs.sig[[clust.key]] <- clust.val
          
          # Run heatmap
          print("_____________")
          print("RUNNING FAIME HEATMAP UNSUPERVISED")
          print("_____________")
          print(str(lArgs.sig))
          print(str(lArgs.scores))
          runHeatmapFaime(lArgs.sig,
                          lArgs.scores)
        } # end iteration over clustering methods
      } # end iteration over reductions
    } # end iteration over transforms
  } # end iteration over ontologies
} # end iteration over data sets
