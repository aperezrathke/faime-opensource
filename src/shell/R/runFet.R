################################################################################################################################################
# Utility to run Fischer Exact Test
################################################################################################################################################

source("./src/shell/R/configPaths.R")

#' Manual fisher exact test
#' @Example
#' --------- Male | Female | Total
#' Diet       a      b        a+b
#' Non-Diet   c      d        c+d
#' Total     a+c    b+d       a+b+c+d=n
fet.man <- function(a.val, b.val, c.val, d.val, ...) {
  library("combinat")
  (nCm(c.val+d.val,c.val)/nCm((a.val+b.val+c.val+d.val),(a.val+c.val))) * nCm(a.val+b.val,a.val)
}

#' Fisher Exact Test, library implementation
fet.lib <- function(a.val, b.val, c.val, d.val, ...) {
  fisher.test(matrix(c(a.val, c.val, b.val, d.val), nr=2), ...)$p.value
}

fet.bridge <- function(val.top.left, sum.top.row, sum.left.col, sum.total, f=fet.man, ...) {
  a.val <- val.top.left
  b.val <- sum.top.row - val.top.left
  c.val <- sum.left.col - val.top.left
  d.val <- sum.total - a.val - b.val - c.val
  f(a.val, b.val, c.val, d.val, ...)
}

fet.gte.man <- function(a.min, sum.top.row, sum.left.col, sum.total) {
  a.max <- min(sum.top.row, sum.left.col)
  sum(simplify2array(lapply(
    X=a.min:a.max,
    FUN=fet.bridge,
    sum.top.row,
    sum.left.col,
    sum.total,
    fet.man), higher=FALSE))
}

fet.gte.lib <- function(a.min, sum.top.row, sum.left.col, sum.total) {
  fet.bridge(a.min, sum.top.row, sum.left.col, sum.total, f=fet.lib, alternative="greater")
}

fet.analyze.term <- function(ontologyId, sigGenes, ontology, backgroundGenes) {
  # Determine set of gene IDs mapped to the current ontology ID
  # - recall 1st column of ontology are ontology IDs, second column are gene IDs 
  geneIdsMappedToTerm <- ontology[which(ontology[,1]==ontologyId),2]
  # Filter out gene IDs mapped to term that are not part of the background gene set
  geneIdsMappedToTerm <- intersect(geneIdsMappedToTerm, backgroundGenes)
  
  # Determine flagged genes in term
  sigGenesInTerm <- intersect(sigGenes, geneIdsMappedToTerm)
  
  # Compute P(i >= a)
  p.val <- fet.gte.lib(
    a.min = length(sigGenesInTerm),
    sum.top.row = length(sigGenes),
    sum.left.col = length(geneIdsMappedToTerm),
    sum.total = length(backgroundGenes)
  )
  
  # Output results
  data.frame("path_id" = ontologyId,
             "p_val" = p.val,
             "num_genes_in_term" = length(geneIdsMappedToTerm),
             "num_sig_genes_in_term" = length(sigGenesInTerm),
             "num_sig_genes" = length(sigGenes),
             "num_background_genes" = length(backgroundGenes))
}

fet.analyze.ontology <- function(pathToSigGenes = NULL,
                                 sigGenes = NULL,
                                 getSigGenesFunc = NULL,
                                 probeIdToGeneIdMap = NULL,
                                 pathToOntology = stop("'pathToOntology' must be specified"),
                                 pathToExpressionData = stop("'pathToExpressionData' must be specified")) {
  # Read ontology
  ontology <- read.table(file=pathToOntology, sep="\t", header=TRUE, colClasses="character")
  # Determine global set of ontology IDs (keys) by capturing unique values in 1st column of ontology
  # - recall 1st column of ontology are ontology IDs, second column are gene IDs
  ontologyIds <- unique(ontology[,1])
  
  # Determine ontology genes
  ontologyGenes <- unique(ontology[,2])
  
  # Read expression data and determine background genes
  backgroundGenes <- rownames(get(load(pathToExpressionData)))
  if (!is.null(probeIdToGeneIdMap)) {
    backgroundGenes <- probeIdToGeneIdMap[backgroundGenes]
  }
  # Filter out measured genes that are not part of the ontology
  backgroundGenes <- intersect(backgroundGenes, ontologyGenes)
  
  if (is.null(pathToSigGenes) && is.null(sigGenes)) {
    stop("Sig Genes or path to Sig Genes must be specified")
  }
  
  # Read list of significant genes
  if (!is.null(pathToSigGenes)) {
    sigGenes <- get(load(pathToSigGenes))
  }
  
  # Extract only list of significant genes
  if (is.null(getSigGenesFunc)) {
    sigGenes <- rownames(sigGenes)
  } else {
    sigGenes <- getSigGenesFunc(sigGenes)
  }
  
  # Filter out significant genes that are not part of the background
  sigGenes <- intersect(sigGenes, backgroundGenes)
  
  # Compute FET for each term
  do.call(rbind, lapply(X=ontologyIds, FUN=fet.analyze.term, sigGenes, ontology, backgroundGenes))
}

#' @return Character vector of the FET class
getFetClass <- function() { "FET" }

#' Utility to sort significant pathways by adjusted p-value
#' @param fet.results Fisher Exact Test results
#' @return FET results ordered by adjusted p-value, rownames are also remapped
orderByAdjPValueAsc.fet <- function(fet.result) {
  # Sort by adjusted p-value
  if (!is.null(fet.result) && nrow(fet.result) > 1) {
    fet.result$adj_p_val <- as.numeric(fet.result$adj_p_val)
    fet.result$p_val <- as.numeric(fet.result$p_val)
    fet.result <- fet.result[with(fet.result, order(adj_p_val, p_val)), ]
    rownames(fet.result) <- 1:nrow(fet.result)
  }
  return(fet.result)
}

#' Sorts pathways and then returns pathways below threshold
#' @param fet.results Fisher Exact Test results
#' @param flt.fdr false discovery rate to filter by
#' @param bIsPreSorted TRUE if we can skip sorting
#' @return fet.results Fisher Exact Test results sorted and containing only those less than flt.fdr
filterByFdr.fet <- function(fet.result, flt.fdr, bIsPreSorted=FALSE) {
  if (!is.null(fet.result)) {
    if (!bIsPreSorted) {
      fet.result <- orderByAdjPValueAsc.fet(fet.result)
    }
    fet.result <- fet.result[as.numeric(fet.result$adj_p_val) < flt.fdr, ]
  }
  return(fet.result)
}

#' Utility function to generate the FET qualifiers
#' @param fdr The threshold false discovery rate
#' @param p.adj.method The method used for estimating the false discovery rate
#' @param dereg Are the genes up or down regulated (or not specified)
#' @param dataSet.inClass The input data set class
#' @param dataSet.inQuals Input qualifiers
#' @param dataSet.inClass Input class
generateQuals.fet <- function(fdr, p.adj.method, dereg, dataSet.inQuals, dataSet.inClass) {
  sprintf("%s%s%s.fdr%s.am%s.de",
          condDashPrefix(condSetDefault(dataSet.inQuals, "")),
          condDashPrefix(condSetDefault(dataSet.inClass, "")),
          condDashPrefix(condSetDefault(fdr, "NA")),
          condDashPrefix(condSetDefault(p.adj.method, "NA")),
          condDashPrefix(condSetDefault(dereg, "NA")))
}

#' Utility function based on path convention for running a FET analysis
#' @param lArgs A list of arguments containing dataSet.name, dataSet.mods, dataSet.inClass, dataSet.inQuals,
#'          input.dir, output.dir, probeIdToGeneIdMap, pathIdToPathNameFunc, out.ontology.name, fet.fdr, fet.p.adj.method, fet.getSigGenesFunc, fet.dereg
#' @return FET scores for arguments
runFet <- function(lArgs) {
  # Make sure FET ontology is specified
  if (is.null(lArgs$out.ontology.name)) {
    stop("FET needs to have out.ontology.name specified")
  }
  
  # Make sure we can extract significant genes list
  if (is.null(lArgs$fet.getSigGenesFunc)) {
    stop("FET needs to have fet.getGenesFunc specified")
  }
  
  # Derive any missing parameters
  lArgs$fet.fdr <- condSetDefault(lArgs$fet.fdr, 0.05)
  lArgs$fet.p.adj.method <- condSetDefault(lArgs$fet.p.adj.method, "none")
  lArgs$dataSet.inClass <- condSetDefault(lArgs$dataSet.inClass, "")
  lArgs$dataSet.inQuals <- condSetDefault(lArgs$dataSet.inQuals, "")
  lArgs$dataSet.outClass <- condSetDefault(lArgs$dataSet.outClass, getFetClass())
  lArgs$dataSet.outQuals <- condSetDefault(lArgs$dataSet.outQuals,
                                           generateQuals.fet(fdr=lArgs$fet.fdr,
                                                             p.adj.method=lArgs$fet.p.adj.method,
                                                             dereg=lArgs$fet.dereg,
                                                             dataSet.inQuals=lArgs$dataSet.inQuals,
                                                             dataSet.inClass=lArgs$dataSet.inClass))
  
  # Run Fisher exact test
  fet.result <- fet.analyze.ontology(pathToSigGenes=getDataSetInputPath(lArgs),
                                     getSigGenesFunc=lArgs$fet.getSigGenesFunc,
                                     probeIdToGeneIdMap=lArgs$probeIdToGeneIdMap,
                                     pathToOntology=getOntologyFilePath(lArgs$out.ontology.name),
                                     pathToExpressionData=getDataSetExpressionDataPath(lArgs))
  
  # Adjust p-values
  adj_p_val <- p.adjust(p=fet.result$p_val, method=lArgs$fet.p.adj.method)
  fet.result <- cbind(fet.result, as.data.frame(adj_p_val))
  
  # Patch up significant genes table for pathways
  if (!is.null(lArgs$pathIdToPathNameFunc)) {
    fet.result <- lArgs$pathIdToPathNameFunc(fet.result, by.y="path_id")
  }
  
  # Sort and filter results
  fet.result <- filterByFdr.fet(fet.result, lArgs$fet.fdr)
  
  # Create output directory if it doesn't exist
  condCreateDir(getParamsOutputDir(lArgs))
  
  # Save result
  save(fet.result, file=getDataSetOutputPath(lArgs))
  return(fet.result)
}
