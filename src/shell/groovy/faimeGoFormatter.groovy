/**
 * Formats ontology files for use with FAIME
 * - Requires .obo file to create a mapping from ontology IDs to ontology meta info
 * - Requires .goa_* (gaf) annotation file to map gene IDs to ontologies
 */

// Parameters

// e.g 'biological_process', 'molecular_function', 'cellular_function'
final String namespaceOfOntology = "biological_process"
final String dateOfOntology = "6_14_12"
final String nameOfOntology = "GO_${namespaceOfOntology}_${dateOfOntology}"
final String pathToResourcesDir = "../resources/ontologies"
final String pathToOboFile = "${pathToResourcesDir}/gene_ontology_ext.obo"
final String pathToGafFile = "${pathToResourcesDir}/gene_association.goa_human"
final String pathToOutputDir = "../../../output"
final String pathToOutputFile = "${pathToOutputDir}/${nameOfOntology}.txt"

// Parse GO terms
final def GO_TERMS = new faimeGoTerms( pathToOboFile )

// Parse GO annotations
final def GO_GENES = new faimeGoGenes( pathToGafFile )

/**
 * @return true if go identifier belongs to proper namespace
 */
final def isGoIdInNamespace = { goId ->
    if ( GO_TERMS.get( goId ) ) {
        GO_TERMS.get( goId )?.namespace == namespaceOfOntology
    } else {
        println "WARNING: $goId not found in GO terms"
    }
}

// Output ontology formatted for use with FAIME
if ( GO_GENES && GO_TERMS ) {
    new File( pathToOutputFile ).withWriter { out ->
        out << "GOID\tGeneSymbol\n"        
        GO_GENES?.each { goId, geneIds ->
            if ( isGoIdInNamespace( goId ) ) {
                geneIds.each { geneId ->                
                    out << "${goId}\t${geneId}\n"
                }
            }
        }
    }
}

