/**
 * Appends simple serialization interface to Map
 */
public class faimeSerializableKeyStore {

    // A mapping from GO identifier to term information (also a map)
    // See http://jira.codehaus.org/browse/GROOVY-3471
    protected @Delegate(interfaces=false) Map keyStore = [:]

    /**
     * @return String representation of ontology terms
     */
    public String toString() {
        keyStore.toString()
    }

    /**
     * Serializes out to file
     * @param filePath Path to serialize to
     */
    public void save( String filePath ) {
        new File( filePath ).withObjectOutputStream { outStream ->
            outStream << keyStore
        }
    }

    /**
     * Serializes from file
     * @param filePath Path to serialize from
     */
    public void load( String filePath ) {
        new File( filePath ).withObjectInputStream { inStream ->
            inStream.eachObject {
                keyStore = inStream
            }
        }
    }

    /**
     * Expose 'each'
     * @param func Closure which takes two parameters: 'key' and 'value'
     */
    public def each( Closure func ) {
        keyStore.each( func )
    }
}

