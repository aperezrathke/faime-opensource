/**
 * Parses the XML phenotype file for GSE36968 dataset and writes
 * out text file that is easily imported to R via read.table()
 */

// Paths
String dataSetName = "GSE36968"
String pathToResourcesDir = "../resources/${dataSetName}"
String pathToFamilyXml = "${pathToResourcesDir}/${dataSetName}_family.xml"
String pathToOutputDir = pathToResourcesDir // "../../../output"
String pathToOutputFile = "${pathToOutputDir}/${dataSetName}-pheno.txt"
final String strNormalId = "1"
final String strDiseaseId = "2"

// Read in XML
def phenoXml = new XmlParser().parse( pathToFamilyXml )

// Mapping from sample identifier to phenotype information
def outPhenos = [:]

/**
 * @return true if sample contains RNA-seq data, false otherwise
 */
def isRnaSeqData = { sample ->
	sample.Title.text().contains("_RNA-seq")
}

// Extract phenotype information for each sample
phenoXml.Sample.each { sample ->
	if ( isRnaSeqData( sample ) ) {
		outPhenos[ sample.@iid ] = [:]
		sample.Channel.Characteristics.each { characteristic ->
			outPhenos[ sample.@iid ][ characteristic.@tag ] = characteristic.text()
		}
        outPhenos[ sample.@iid ][ "SamClass" ] =
            outPhenos[ sample.@iid ].Stage.contains("Normal") ? strNormalId : strDiseaseId
        outPhenos[ sample.@iid ][ "TwilightClass" ] = outPhenos[ sample.@iid ][ "SamClass" ]
	}
}

/**
 * Writes a row of parameter attribute values for each sample
 */
def writeAttrib = { targetAttribKey, out ->
	def idxSample = 0
	outPhenos.each { id, attribs ->
		if ( idxSample == 0 ) {
			out.write( targetAttribKey )
		}
		idxSample++
		attribs.each { attribKey, attribValue ->
			if ( attribKey == targetAttribKey ) {
				out.write( "\t" + attribValue )
			}
		}
	}
}

// Write R formatted phenotype information to disk
new File( pathToOutputFile ).withWriter { out ->
	// Write sample names as header
	def idxSample = 0
	outPhenos.each { id, attribs ->
		if ( idxSample == 0 ) {
			out.write(id)
		} else {
			out.write("\t" + id)
		}
		idxSample++
	}
	out.write("\n")
	
    // Write attributes for each row
	outPhenos.iterator()?.next()?.value?.keySet().each { targetAttribKey ->
		writeAttrib( targetAttribKey, out )
		out.write("\n")
	}
}

// Write GSEA phenotype .cls file
final String pathToGseaClsFile = "${pathToResourcesDir}/gsea/${dataSetName}.cls"
final String strDiseaseLabel = "Disease"
final String strNormalLabel = "Normal"

// A utility to extract the reference phenotype value for parameter sample
def getReferencePhenotype = { aSampleId ->
    outPhenos[aSampleId].TwilightClass
}
    
new File( pathToGseaClsFile ).withWriter { out ->
    // Capture iteration order of the keys
    def keys = []
    outPhenos.each { id, attribs ->
        keys << id
    }
    
    // The first line of a CLS file contains numbers indicating the number of samples and number of classes.
    // The number of samples should correspond to the number of samples in the associated RES or GCT data file.
    // Line format:      (number of samples) (space) (number of classes) (space) 1
    final def numberOfClasses = 2
    out.write("${outPhenos.size()} ${numberOfClasses} 1\n")
    
    // The second line in a CLS file contains a user-visible name for each class. 
    // These are the class names that appear in analysis reports.
    // The line should begin with a pound sign (#) followed by a space.
    // Line format:      # (space) (class 0 name) (space) (class 1 name)
    final String firstClassEncountered = (getReferencePhenotype(keys[0]) == strDiseaseId) ?
        strDiseaseLabel : strNormalLabel
    final String secondClassEncountered = (firstClassEncountered == strDiseaseLabel) ?
        strNormalLabel : strDiseaseLabel 
    out.write("# ${firstClassEncountered} ${secondClassEncountered}\n")
    
    // The third line contains a class label for each sample.
    // The class label can be the class name, a number, or a text string.
    // The first label used is assigned to the first class named on the second line; 
    // the second unique label is assigned to the second class named; and so on.
    // (Note: The order of the labels determines the association of class names and class labels,
    // even if the class labels are the same as the class names.)
    // The number of class labels specified on this line should be the same as the number of samples specified in the first line.
    // The number of unique class labels specified on this line should be the same as the number of classes specified in the first line.
    // Line format:      (sample 1 class) (space) (sample 2 class) (space) ... (sample N class)
    def idxSample = 0
    for (key in keys) {
        if ( idxSample == 0 ) {
            out.write(getReferencePhenotype(key))
        } else {
            out.write(" " + getReferencePhenotype(key))
        }
        ++idxSample
    }
}

