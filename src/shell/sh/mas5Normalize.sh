#!/bin/bash

SH_SCRIPT_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DATA_NAME=$1
DATA_BASE_DIR=$SH_SCRIPT_DIR/../resources/$DATA_NAME
DATA_CEL_DIR=$DATA_BASE_DIR/CEL
DATA_RDATA_NAME=$DATA_NAME-mas5.rdata
DATA_RDATA_PATH=$DATA_BASE_DIR/$DATA_RDATA_NAME

R_SCRIPT_NAME=affyCelToMas5Util.R
R_SCRIPT_DIR=$SH_SCRIPT_DIR/../R
R_SCRIPT_PATH=$R_SCRIPT_DIR/$R_SCRIPT_NAME

MAX_ROWS=0
MAX_COLS=0

echo .CEL path: $DATA_CEL_DIR
echo .rdata path: $DATA_RDATA_PATH
echo .R path: $R_SCRIPT_PATH
echo max rows: $MAX_ROWS
echo max cols: $MAX_COLS
echo Begin mas5 normalize.
Rscript $R_SCRIPT_PATH $DATA_CEL_DIR $DATA_RDATA_PATH $MAX_ROWS $MAX_COLS
echo Finish mas5 mormalize.

